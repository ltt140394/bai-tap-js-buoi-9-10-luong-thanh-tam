function NhanVien(_taiKhoanNV, _tenNV, _emailNV, _matKhauNV, _ngayLam, _luongCoBan, _chucVu, _gioLam) {
    this.taiKhoanNV = _taiKhoanNV;
    this.tenNV = _tenNV;
    this.emailNV = _emailNV;
    this.matKhauNV = _matKhauNV;
    this.ngayLam = _ngayLam;
    this.luongCoBan = _luongCoBan;
    this.chucVu = _chucVu;
    this.gioLam = _gioLam;

    this.tinhTongLuong = function () {
        if (this.chucVu == "Sếp") {
            return new Intl.NumberFormat('vn-VN').format(this.luongCoBan * 3);
        } else if (this.chucVu == "Trưởng phòng") {
            return new Intl.NumberFormat('vn-VN').format(this.luongCoBan * 2);
        } else {
            return new Intl.NumberFormat('vn-VN').format(this.luongCoBan);
        }
    }

    this.xepLoai = function () {
        if (this.gioLam >= 192) {
            return "Xuất Sắc";
        } else if (this.gioLam >= 176) {
            return "Giỏi";
        } else if (this.gioLam >= 160) {
            return "Khá";
        } else {
            return "Trung Bình";
        }
    }
}
