function xoaDauTiengViet(str) {
    return str.normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '')
        .replace(/đ/g, 'd').replace(/Đ/g, 'D');
}

function ValidatorNV() {
    this.kiemTraRong = function (idTarget, idError, messageError) {
        var valueTarget = document.getElementById(idTarget).value.trim();
        if (!valueTarget) {
            document.getElementById(idError).innerText = messageError;
            return false;
        } else {
            document.getElementById(idError).innerText = "";
            return true;
        };
    }

    this.kiemTraDoDaiTaiKhoan = function (idTarget, idError) {
        var valueTarget = document.getElementById(idTarget).value.trim();
        if (valueTarget.length >= 4 && valueTarget.length <= 6) {
            document.getElementById(idError).innerText = "";
            return true;
        } else {
            document.getElementById(idError).innerText = "Tài khoản tối thiểu 4 ký tự và tối đa 6 ký tự. Vui lòng nhập lại"
            return false;
        };
    }

    this.kiemTraTrungTaiKhoan = function (idTarget, array) {
        var valueTarget = document.getElementById(idTarget).value.trim();
        var index = array.findIndex(function (item) {
            return item.taiKhoanNV == valueTarget;
        });

        if (index == -1) {
            document.getElementById("tbTKNV").innerText = "";
            return true;
        } else {
            document.getElementById("tbTKNV").innerText = "Tài khoản không được trùng";
            return false;
        };
    }

    this.kiemTraTenHopLe = function (idTarget, idError) {
        var valueTarget = document.getElementById(idTarget).value.trim();
        var chuoiSauKhiXoaDau = xoaDauTiengViet(valueTarget);
        var regex = /^[a-zA-Z_ ]+$/;

        if (chuoiSauKhiXoaDau.match(regex)) {
            document.getElementById(idError).innerText = "";
            return true;
        } else {
            document.getElementById(idError).innerText = "Tên không hợp lệ";
            return false;
        };
    }

    this.kiemTraEmailHopLe = function (idTarget, idError) {
        var valueTarget = document.getElementById(idTarget).value.trim();
        var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if (valueTarget.match(regex)) {
            document.getElementById(idError).innerText = "";
            return true;
        } else {
            document.getElementById(idError).innerText = "Email không hợp lệ";
            return false;
        };
    }

    this.kiemTraMatKhauHopLe = function (idTarget, idError) {
        var valueTarget = document.getElementById(idTarget).value.trim();
        var regex = /^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,10}$/;

        if (valueTarget.match(regex)) {
            document.getElementById(idError).innerText = "";
            return true;
        } else {
            document.getElementById(idError).innerText = "Mật khẩu không hợp lệ";
            return false;
        };
    }

    this.kiemTraNgayHopLe = function (idTarget, idError) {
        var valueTarget = document.getElementById(idTarget).value.trim();
        var regex = /^\d{2}\/\d{2}\/\d{4}$/;

        if (valueTarget.match(regex)) {
            document.getElementById(idError).innerText = "";
            return true;
        } else {
            document.getElementById(idError).innerText = "Ngày không hợp lệ";
            return false;
        };
    }

    this.kiemTraLuongCBHopLe = function (idTarget, idError) {
        var valueTarget = document.getElementById(idTarget).value * 1;
        if (valueTarget >= 1e+6 && valueTarget <= 20e+6) {
            document.getElementById(idTarget).innerText = "";
            return true;
        } else {
            document.getElementById(idError).innerText = "Lương không hợp lệ. Lương phải trong khoảng 1 triệu - 20 triệu";
            return false;
        };
    }

    this.kiemTraChucVuHopLe = function (idTarget, idError) {
        var valueTarget = document.getElementById(idTarget);

        if (valueTarget.selectedIndex == 0) {
            document.getElementById(idError).innerText = "Phải chọn 1 chức vụ";
            return false;
        } else {
            document.getElementById(idError).innerText = "";
            return true;
        };
    }

    this.kiemTraGioLamHopLe = function (idTarget, idError) {
        var valueTarget = document.getElementById(idTarget).value;
        if (valueTarget >= 80 && valueTarget <= 200) {
            document.getElementById(idError).innerText = "";
            return true;
        } else {
            document.getElementById(idError).innerText = "Số giờ làm không hợp lệ. Số giờ làm phải trong khoảng 80-200 giờ";
            return false;
        };
    }
}

