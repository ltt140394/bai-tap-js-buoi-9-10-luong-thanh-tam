var danhSachNhanVien = [];

var validatorNV = new ValidatorNV();

const DSNV_LOCALSTORAGE = "DSNV_LOCALSTORAGE";

//Convert dữ liệu từ json và gán trở lại arr gốc khi load trang
var dsnvArrGoc = localStorage.getItem(DSNV_LOCALSTORAGE);
if (dsnvArrGoc) {
    danhSachNhanVien = JSON.parse(dsnvArrGoc);
    danhSachNhanVien = danhSachNhanVien.map(function (item) {
        return new NhanVien(item.taiKhoanNV, item.tenNV, item.emailNV, item.matKhauNV, item.ngayLam, item.luongCoBan, item.chucVu, item.gioLam)
    });
    hienThiDanhSachNhanVien(danhSachNhanVien);
}


//Hàm lưu arr thành json để lưu vào local storage
function luuLocalStorage() {
    var dsnvJson = JSON.stringify(danhSachNhanVien);
    localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
}

function themNhanVien() {
    var newNhanVien = layThongTinTuForm();

    var isValidAll;

    var isValidTaiKhoan = validatorNV.kiemTraRong("tknv", "tbTKNV", "Tài khoản không được để trống") && validatorNV.kiemTraTrungTaiKhoan("tknv", danhSachNhanVien) && validatorNV.kiemTraDoDaiTaiKhoan("tknv", "tbTKNV");
    var isValidTen = validatorNV.kiemTraRong("name", "tbTen", "Tên không được để trống") && validatorNV.kiemTraTenHopLe("name", "tbTen");
    var isValidEmail = validatorNV.kiemTraRong("email", "tbEmail", "Email không được để trống") && validatorNV.kiemTraEmailHopLe("email", "tbEmail");
    var isValidMatKhau = validatorNV.kiemTraRong("password", "tbMatKhau", "Mật khẩu không được để trống") && validatorNV.kiemTraMatKhauHopLe("password", "tbMatKhau");
    var isValidNgay = validatorNV.kiemTraRong("datepicker", "tbNgay", "Ngày không được để trống") && validatorNV.kiemTraNgayHopLe("datepicker", "tbNgay");
    var isValidLuongCB = validatorNV.kiemTraRong("luongCB", "tbLuongCB", "Lương không được để trống") && validatorNV.kiemTraLuongCBHopLe("luongCB", "tbLuongCB");
    var isValidChucVu = validatorNV.kiemTraChucVuHopLe("chucvu", "tbChucVu");
    var isValidGioLam = validatorNV.kiemTraRong("gioLam", "tbGiolam", "Số giờ làm không được để trống") && validatorNV.kiemTraGioLamHopLe("gioLam", "tbGiolam");

    isValidAll = isValidTaiKhoan && isValidTen && isValidEmail && isValidMatKhau && isValidNgay && isValidLuongCB && isValidChucVu && isValidGioLam;
    console.log(isValidAll);

    if (isValidAll) {
        danhSachNhanVien.push(newNhanVien);
        hienThiDanhSachNhanVien(danhSachNhanVien);
        resetInputValue();
        luuLocalStorage();
        document.getElementById("btnThemNV").setAttribute("data-dismiss", "modal");
    } else {
        document.getElementById("btnThemNV").removeAttribute("data-dismiss");
    };
}

function xoaNhanVien(id) {
    var viTri = timKiemViTri(id, danhSachNhanVien);
    danhSachNhanVien.splice(viTri, 1);
    hienThiDanhSachNhanVien(danhSachNhanVien);
    luuLocalStorage();
}

function suaNhanVien(id) {
    var viTri = timKiemViTri(id, danhSachNhanVien);
    resetInputValue();
    xuatThongTinLenForm(danhSachNhanVien[viTri]);
}

function capNhatNhanVien() {
    var isValidAll;

    var isValidTaiKhoan = validatorNV.kiemTraRong("tknv", "tbTKNV", "Tài khoản không được để trống");
    var isValidTen = validatorNV.kiemTraRong("name", "tbTen", "Tên không được để trống") && validatorNV.kiemTraTenHopLe("name", "tbTen");
    var isValidEmail = validatorNV.kiemTraRong("email", "tbEmail", "Email không được để trống") && validatorNV.kiemTraEmailHopLe("email", "tbEmail");
    var isValidMatKhau = validatorNV.kiemTraRong("password", "tbMatKhau", "Mật khẩu không được để trống") && validatorNV.kiemTraMatKhauHopLe("password", "tbMatKhau");
    var isValidNgay = validatorNV.kiemTraRong("datepicker", "tbNgay", "Ngày không được để trống") && validatorNV.kiemTraNgayHopLe("datepicker", "tbNgay");
    var isValidLuongCB = validatorNV.kiemTraRong("luongCB", "tbLuongCB", "Lương không được để trống") && validatorNV.kiemTraLuongCBHopLe("luongCB", "tbLuongCB");
    var isValidChucVu = validatorNV.kiemTraChucVuHopLe("chucvu", "tbChucVu");
    var isValidGioLam = validatorNV.kiemTraRong("gioLam", "tbGiolam", "Số giờ làm không được để trống") && validatorNV.kiemTraGioLamHopLe("gioLam", "tbGiolam");

    isValidAll = isValidTaiKhoan && isValidTen && isValidEmail && isValidMatKhau && isValidNgay && isValidLuongCB && isValidChucVu && isValidGioLam;
    console.log(isValidAll);

    if (isValidAll) {
        var nhanVienDuocSua = layThongTinTuForm();
        var viTri = timKiemViTri(nhanVienDuocSua.taiKhoanNV, danhSachNhanVien);
        danhSachNhanVien[viTri] = nhanVienDuocSua;
        hienThiDanhSachNhanVien(danhSachNhanVien);
        luuLocalStorage();
        resetInputValue();
        document.getElementById("btnCapNhat").setAttribute("data-dismiss", "modal");
    } else {
        document.getElementById("btnCapNhat").removeAttribute("data-dismiss");
    };
}

function timNhanVien() {
    var danhSachNhanVienCanTim = [];

    var nhanVienCanTim = document.getElementById("searchName").value.trim();

    danhSachNhanVien.forEach(function (item) {
        if (item.xepLoai() == nhanVienCanTim) {
            danhSachNhanVienCanTim.push(item);
        };
    });

    hienThiDanhSachNhanVien(danhSachNhanVienCanTim);
}

function hienThiLaiDSNVKhiXoaTextSearch() {
    var valueTextSearch = document.getElementById("searchName").value;
    if (valueTextSearch == "") {
        hienThiDanhSachNhanVien(danhSachNhanVien);
    };
}