function layThongTinTuForm() {
    var taiKhoanNV = document.getElementById("tknv").value;
    var tenNV = document.getElementById("name").value;
    var emailNV = document.getElementById("email").value;
    var matKhauNV = document.getElementById("password").value;
    var ngayLam = document.getElementById("datepicker").value;
    var luongCoBan = document.getElementById("luongCB").value * 1;
    var chucVu = document.getElementById("chucvu").value;
    var gioLam = document.getElementById("gioLam").value * 1;

    var nhanVien = new NhanVien(taiKhoanNV, tenNV, emailNV, matKhauNV, ngayLam, luongCoBan, chucVu, gioLam);

    return nhanVien;
}

function hienThiDanhSachNhanVien(dsnv) {
    var contentHTML = "";

    for (i = 0; i < dsnv.length; i++) {
        var nhanVien = dsnv[i];
        var contentTr = /*html */ `
        <tr>
        <td>${nhanVien.taiKhoanNV}</td>
        <td>${nhanVien.tenNV}</td>
        <td>${nhanVien.emailNV}</td>
        <td>${nhanVien.ngayLam}</td>
        <td>${nhanVien.chucVu}</td>
        <td>${nhanVien.tinhTongLuong()}</td>
        <td>${nhanVien.xepLoai()}</td>
        <td>
        <button class="btn btn-danger"  onclick="xoaNhanVien('${nhanVien.taiKhoanNV}')">Xóa</button>
        <button class="btn btn-success" data-toggle="modal" data-target="#myModal" onclick="suaNhanVien('${nhanVien.taiKhoanNV}')">Sửa</button>
        </td>
        </tr>
        `
        contentHTML += contentTr;
    }

    document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function timKiemViTri(id, array) {
    return array.findIndex(function (item) {
        return item.taiKhoanNV == id;
    });
}

function xuatThongTinLenForm(nv) {
    document.getElementById("tknv").value = nv.taiKhoanNV;
    document.getElementById("name").value = nv.tenNV;
    document.getElementById("email").value = nv.emailNV;
    document.getElementById("password").value = nv.matKhauNV;
    document.getElementById("datepicker").value = nv.ngayLam;
    document.getElementById("luongCB").value = nv.luongCoBan;
    document.getElementById("chucvu").value = nv.chucVu;
    document.getElementById("gioLam").value = nv.gioLam;
}

function resetInputValue() {
    document.getElementById("tknv").value = "";
    document.getElementById("name").value = "";
    document.getElementById("email").value = "";
    document.getElementById("password").value = "";
    document.getElementById("datepicker").value = "";
    document.getElementById("luongCB").value = "";
    document.getElementById("chucvu").value = "";
    document.getElementById("gioLam").value = "";
}

